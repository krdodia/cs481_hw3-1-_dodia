﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MainPage)}: ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        async void OnTopStoriesPage(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnTopStoriesPage)}");

            string response = await DisplayActionSheet("OverloadedActionSheet", "Cancel", null,"Headlines", "US", "World", "For You");
            Debug.WriteLine($"User picked:  {response}");

            if (response.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new TopStoriesPage("Man Charged in killing of DC Woman.."));
            }
            if (response.Equals("For You", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new TopStoriesPage("Watch How Tesla model earned ratings on safety"));
            }
        }

        void OnSportsPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSportsPage)}");
            Navigation.PushAsync(new SportsPage());
        }

        void OnTechnologyPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnTechnologyPage)}");
            Navigation.PushAsync(new TechnologyPage());
        }
    }
}
