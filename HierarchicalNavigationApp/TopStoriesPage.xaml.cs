﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class TopStoriesPage : ContentPage
    {
        public TopStoriesPage(string newsOfUS)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TopStoriesPage)}: ctor");
            InitializeComponent();

            newsLabel.Text = newsOfUS;
        }
    }
}
