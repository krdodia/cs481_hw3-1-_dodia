﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNavigationApp
{
    public partial class TechnologyPage : ContentPage
    {
        public TechnologyPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TechnologyPage)}: ctor");
            InitializeComponent();
        }
        protected void OnClicked(object sender,System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.pcmag.com/news/363892/google-apps-can-scan-and-share-your-gmail-data-with-consen"));
        }
    }
}
